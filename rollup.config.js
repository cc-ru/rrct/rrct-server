import { terser } from "rollup-plugin-terser";

module.exports = {
  input: 'src/rrct.js',
  output: {
    file: 'static/js/rrct.js',
    format: 'iife'
  },
  plugins: [ terser() ]
};
