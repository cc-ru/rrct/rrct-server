// helpers
// --------------------------------------------------------------------------------- //
var utf8encoder = new TextEncoder('utf-8');
var utf8decoder = new TextDecoder('utf-8');

function encode(str) {
  return utf8encoder.encode(str);
}

function decode(array) {
  return utf8decoder.decode(array);
}

// network
// --------------------------------------------------------------------------------- //
var socket, listener;

var pkg = {
  message: 0,
  subscribe: 1,
  unsubscribe: 2,
  ping: 3,
  pong: 4
}

function setSize(array, size) {
  array[0] = Math.floor(size / 256);
  array[1] = size % 256;
}

function setType(array, type) {
  array[2] = type;
}

function setIDLength(array, length) {
  array[3] = length;
}

function setID(array, encodedId) {
  array.set(encodedId, 4);
}

function packageMessage(id, message) {
  var encodedId = encode(id);
  var idLen = encodedId.length;
  var encodedMessage = encode(message);
  var size = 1 + 1 + idLen + encodedMessage.length;
  var array = new Uint8Array(size + 2);
  setSize(array, size);
  setType(array, pkg.message);
  setIDLength(array, idLen);
  setID(array, encodedId);
  var offset = 2 + 1 + 1 + idLen;
  array.set(encodedMessage, offset);
  return array;
}

function packageSubscribe(id) {
  var encodedId = encode(id);
  var idLen = encodedId.length;
  var size = 1 + 1 + idLen;
  var array = new Uint8Array(size + 2);
  setSize(array, size);
  setType(array, pkg.subscribe);
  setIDLength(array, idLen);
  setID(array, encodedId);
  return array;
}

var pingContent = new Uint8Array(256);
window.crypto.getRandomValues(pingContent);
function packagePing() {
  var size = 1 + pingContent.length;
  var array = new Uint8Array(size + 2);
  setSize(array, size);
  setType(array, pkg.ping);
  array.set(pingContent, 3);
  return array;
}

function packagePong(content) {
  var size = 1 + content.length;
  var array = new Uint8Array(size + 2);
  setSize(array, size);
  setType(array, pkg.pong);
  array.set(content, 3);
  return array;
}

function subscribeOnSocketEvents() {
  socket.onmessage = function (event) {
    if (event.data instanceof ArrayBuffer) {
      var array = new Uint8Array(event.data);
      // [01 len] [2 type]...
      if (array[2] == pkg.message) {
        // ..[3 channel ID len] [channel ID] [content]
        let channelLen = array[3];
        let channelId = decode(array.slice(4, 4 + channelLen))
        let message = decode(array.slice(4 + channelLen, array.length));
        if (listener) listener(channelId, message);
      } else if (array[2] == pkg.ping) {
        // .. [3+ ping content]
        // keep the connection alive by responding to the ping messages
        socket.send(packagePong(array.slice(3, array.length)));
      }
    }
  };
  socket.onopen = function() {
    console.log("[INFO] Succesfully connected to STEM server.");
  };
  socket.onclose = function() {
    reconnect();
  }
  socket.onerror = function() {
    reconnect();
  }
}

function reconnect() {
  console.log("[INFO] Reconnecting to the STEM server...");
  connect();
}

function connect() {
  socket = new WebSocket("wss://stem.fomalhaut.me/ws");
  socket.binaryType = "arraybuffer";
  subscribeOnSocketEvents();
}

function subscribe(channel) {
  socket.send(packageSubscribe(channel));
}

function listen(callback) {
  listener = callback;
}

function send(channelId, message) {
  socket.send(packageMessage(channelId, message));
}

export { connect, subscribe, listen, send }
