// Remote Robot Control Tool
// v 0.1.0
// 2020 (c) Totoro

import * as STEM from "./stem.js";
import { OrbitControls } from "./orbit.js";
import { Robot } from "./robot.js";

let assets = {}, scene, camera, controls, renderer, clock;
let robot;

function load() {
  // load assets
  var loader = new GLTFLoader();
  loader.load('./models/robot.glb', function (gltf) {
    assets.robot = gltf.scene;
    // let's go
    init();
    update();
    //
  }, undefined, function (error) {
    document.getElementById("splash-init").style.display = "none";
    document.getElementById("splash-error").style.display = "block";
    console.log("[ERROR] Cannot load the robot model...");
    console.error(error);
  });
}

function init() {
  // init graphics stuff and the scene
  scene = new THREE.Scene();
  camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 0.1, 1000);
  camera.position.set(0, 2, -5);

  renderer = new THREE.WebGLRenderer({ alpha: true });
  renderer.outputEncoding = THREE.sRGBEncoding;
  renderer.setSize(window.innerWidth, window.innerHeight);
  document.body.appendChild(renderer.domElement);

  controls = new OrbitControls(camera, renderer.domElement);
  controls.enableDamping = true;
  controls.dampingFactor = 0.05;
  controls.screenSpacePanning = false;
  controls.minDistance = 2;
  controls.maxDistance = 50;

  var light = new THREE.AmbientLight(0xeeeeee);
  scene.add(light);

  assets.robot.scale.set(0.5, 0.5, 0.5);
  scene.add(assets.robot);
  robot = new Robot(assets.robot);

  // event listeners
  window.addEventListener("resize", function(event) {
    renderer.setSize(window.innerWidth, window.innerHeight);
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
  });

  window.addEventListener('keydown', function(event) {
    switch (event.keyCode) {
      case 87: // w
        if (!robot.isMoving()) robot.forward();
        break;
      case 65: // a
        if (!robot.isMoving()) robot.turnLeft();
        break;
      case 83: // s
        if (!robot.isMoving()) robot.back();
        break;
      case 68: // d
        if (!robot.isMoving()) robot.turnRight();
        break;
      case 81: // q
        if (!robot.isMoving()) robot.turnAround();
        break;
      case 69: // e
        // TODO
        break;
    }
  }, false);

  // init the rest
  clock = new THREE.Clock();
  document.getElementById("splash-init").style.display = "none";
  STEM.connect();
}

function update() {
  requestAnimationFrame(update);
  let dt = clock.getDelta();

  controls.update();
  robot.update(dt);

  renderer.render(scene, camera);
};

// kick off
load();
