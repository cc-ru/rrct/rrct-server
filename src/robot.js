const MOVEMENT_SPEED = 2.0;
const ROTATION_SPEED = 2.0;

// Robot directions:
//        z
//        ^
//        0
//  x < 3 • 1
//        2
//

function mod(a, n) {
  return a - Math.floor(a/n) * n;
}

function minDirDelta(a, b) {
  return mod(b - a + 2, 4) - 2;
}


export class Robot {
  constructor(model) {
    this.x = 0;
    this.y = 0;
    this.z = 0;
    this.targetX = 0;
    this.targetY = 0;
    this.targetZ = 0;
    this.dir = 0;
    this.targetDir = 0;
    this.model = model;
  }

  isMoving() {
    return this.targetX != this.x || this.targetY != this.y || this.targetZ != this.z || this.targetDir != this.dir;
  }

  forward() {
    switch (this.dir) {
      case 0: this.targetZ = this.z + 1; break;
      case 1: this.targetX = this.x - 1; break;
      case 2: this.targetZ = this.z - 1; break;
      case 3: this.targetX = this.x + 1; break;
    }
  }

  back() {
    switch (this.dir) {
      case 0: this.targetZ = this.z - 1; break;
      case 1: this.targetX = this.x + 1; break;
      case 2: this.targetZ = this.z + 1; break;
      case 3: this.targetX = this.x - 1; break;
    }
  }

  up() {
    this.targetY = this.y + 1;
  }

  down() {
    this.targetY = this.y - 1;
  }

  turnLeft() {
    this.targetDir = (this.dir - 1 + 4) % 4;
  }

  turnRight() {
    this.targetDir = (this.dir + 1) % 4;
  }

  turnAround() {
    this.targetDir = (this.dir + 2) % 4;
  }

  __trans(a, b, speed, dt, deltaFn) {
    let delta = deltaFn ? deltaFn(a, b) : b - a;
    if (delta < 0) {
      return a + Math.max(delta, -speed * dt);
    } else {
      return a + Math.min(delta, speed * dt);
    }
  }

  update(dt) {
    let moved = false;
    if (this.targetX != this.x) {
      this.x = this.__trans(this.x, this.targetX, MOVEMENT_SPEED, dt)
      moved = true;
    }
    if (this.targetY != this.y) {
      this.y = this.__trans(this.y, this.targetY, MOVEMENT_SPEED, dt)
      moved = true;
    }
    if (this.targetZ != this.z) {
      this.z = this.__trans(this.z, this.targetZ, MOVEMENT_SPEED, dt)
      moved = true;
    }
    if (this.targetDir != this.dir) {
      this.dir = (this.__trans(this.dir, this.targetDir, ROTATION_SPEED, dt, minDirDelta) + 4) % 4;
      moved = true;
    }
    if (moved) {
      this.model.position.set(this.x, this.y, this.z);
      this.model.rotation.set(0, -this.dir * Math.PI / 2, 0);
    }
  }
}
