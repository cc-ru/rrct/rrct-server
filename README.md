![RRCT Logo](static/images/logo.png)

# Remote Robot Control Tool
Handy web interface to manage your robots.

# Build
You will need [Rollup.JS](https://rollupjs.org/guide/en/) to build this project.

Compile Three.JS dependency:
```bash
rollup -c libs.rollup.config.js
```
Then compile the RRCT itself:
```bash
rollup -c
```

Now you can take the content of `/static` directory and host it by any static web-server.
