import { terser } from "rollup-plugin-terser";

module.exports = {
  input: 'src/three/three.js',
  output: {
    file: 'static/js/three.js',
    format: 'iife'
  },
  plugins: [ terser() ]
};
